export type Position = 0 | 1 | 2 | 3
export type Trumpf = 'E' | 'L' | 'H' | 'S' | 'G' | 'B' | 'KU' | 'KO'

export interface PlayerDto extends TablePlayerDto {
    weis?: Array<string>
    cards?: Array<string>
}

interface WeisPoints {
    points: number
    stoecke: boolean
}

export interface GameDto {
    currentMove?: number
    trumpf?: Trumpf
    points?: [number, number]
    weisPoints?: [WeisPoints, WeisPoints]
    round: Array<string>
    lastRound?: {
        startPosition: number
        cards: [string, string, string, string]
    }
    undoable: boolean
    players: Array<PlayerDto | null>
}

export interface TablePlayerDto {
    name: string
    position: Position
}

export interface TableDto {
    id: string
    protected: boolean
    players: Array<TablePlayerDto>
}

export interface CreateRequestBody {
    playerid: string
    name: string
    password?: string
}

export interface CreateResponseBody {
    id: string
}

export interface JoinRequestBody {
    playerid: string
    name: string
    position: Position
    password?: string
}

export interface TrumpfRequestBody {
    playerid: string
    trumpf: Trumpf
}

export interface WeisRequestBody {
    playerid: string
    cards: Array<string>
}

export interface StoeckeRequestBody {
    playerid: string
}

export interface PlayRequestBody {
    playerid: string
    card: string
}

export interface PlayRequestBody {
    playerid: string
    card: string
}

export interface UndoRequestBody {
    playerid: string
}

export interface NewRequestBody {
    playerid: string
}
