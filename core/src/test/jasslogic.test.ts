import { expect, use, should } from 'chai'
import ChaiArrays = require('chai-arrays');
import 'mocha';
import {roundWinner, cardsToWeis, calcPoints, cardAllowed, compareWeis} from '../jasslogic'

use(ChaiArrays)
should()

describe('RoundWinner', () => {
    it('Trumpf U should win the trumpf round', () => {
        expect(roundWinner(["HK", "H9", "HU", "HA"], "H")).to.equal(2)
    })
    it('Trumpf 9 should win the trumpf round', () => {
        expect(roundWinner(["H6", "H9", "HK", "HA"], "H")).to.equal(1)
    })
    it('Trumpf makes a stich', () => {
        expect(roundWinner(["H6", "HA", "E6", "H9"], "E")).to.equal(2)
    })
    it('Second trumpf makes a stich', () => {
        expect(roundWinner(["H6", "HA", "E6", "E9"], "E")).to.equal(3)
    })
    it('Highest card without trumpf makes stich', () => {
        expect(roundWinner(["H6", "HA", "HX", "H9"], "E")).to.equal(1)
    })
    it('Last of color makes stich', () => {
        expect(roundWinner(["H6", "SA", "SX", "S9"], "E")).to.equal(0)
    })
    it('6 makes makes G stich', () => {
        expect(roundWinner(["H6", "HA", "HX", "H9"], "G")).to.equal(0)
    })
    it('6 makes makes KU stich', () => {
        expect(roundWinner(["H6", "HA", "HX", "H9"], "KU")).to.equal(0)
    })
    it('E7 makes G stich with S6 in round', () => {
        expect(roundWinner(["E7", "S6", "E9", "EU"], "G")).to.equal(0)
    })
    it('A makes makes B stich', () => {
        expect(roundWinner(["H6", "HA", "HX", "H9"], "B")).to.equal(1)
    })
    it('HK makes makes B stich with SA in round', () => {
        expect(roundWinner(["H6", "SA", "HK", "H9"], "B")).to.equal(2)
    })
})

describe('CardsToWeis', () => {
    it('Stoecke', () => {
        expect(cardsToWeis(["SO", "SK"], "S")).to.be.equalTo([])
    })
    it('Stoecke and 3 trumpf weis', () => {
        const result = cardsToWeis(["SO", "SK", "SU"], "S")
        expect(result).to.be.containingAllOf(["3SK"])
        expect(result).to.be.lengthOf(1)
    })
    it('Quartett', () => {
        expect(cardsToWeis(["SO", "EO", "LO", "HO"], "S")).to.be.equalTo(["QO"])
    })
    it('Overlapping quartett and straight of 4 returns quartett', () => {
        expect(cardsToWeis(["SO", "EO", "LO", "HO", "SX", "SU", "SK"], "E")).to.be.equalTo(["QO"])
    })
    it('Overlapping quartett and straight of 5 returns straight of 5', () => {
        expect(cardsToWeis(["SO", "EO", "LO", "HO", "SX", "SU", "SK", "SA"], "E")).to.be.equalTo(["5SA"])
    })
    it('Overlapping QU and straight of 5 returns QU', () => {
        expect(cardsToWeis(["SU", "EU", "LU", "HU", "SX", "SO", "SK", "SA"], "E")).to.be.equalTo(["QU"])
    })
    it('Two 4s in same color return both', () => {
        const result = cardsToWeis(["L6", "L7", "L8", "L9", "LU", "LO", "LK", "LA"], "E")
        expect(result).to.be.containingAllOf(["4L9", "4LA"])
        expect(result).to.be.lengthOf(2)
    })
    it('Quartett of 8 does not count when played E', () => {
        expect(cardsToWeis(["S8", "E8", "H8", "L8"], "E")).to.be.equalTo([])
    })
    it('Quartett of 8 does count when played G', () => {
        expect(cardsToWeis(["S8", "E8", "H8", "L8"], "G")).to.be.equalTo(["Q8"])
    })
})

describe('CalcPoints', () => {
    it('Empyt stich', () => {
        expect(calcPoints(["S6", "S8", "S9", "S7"], "E")).to.equal(0)
    })
    it('Sack stich', () => {
        expect(calcPoints(["EU", "S8", "S9", "S7"], "E")).to.equal(20)
    })
    it('Max stich', () => {
        expect(calcPoints(["SU", "S9", "SA", "EA"], "S")).to.equal(56)
    })
    it('Stich with 8 in G', () => {
        expect(calcPoints(["S8", "S6", "E6", "L6"], "G")).to.equal(8)
    })
    it('U and 9 not in trumpf', () => {
        expect(calcPoints(["SU", "S9", "S6", "E6"], "E")).to.equal(2)
    })
    it('Single Nell', () => {
        expect(calcPoints(["S6", "S7", "S8", "E9"], "E")).to.equal(14)
    })
    it('Single X', () => {
        expect(calcPoints(["S6", "S7", "S8", "SX"], "E")).to.equal(10)
    })
    it('Single U', () => {
        expect(calcPoints(["S6", "S7", "S8", "SU"], "E")).to.equal(2)
    })
    it('Single O', () => {
        expect(calcPoints(["S6", "S7", "S8", "SO"], "E")).to.equal(3)
    })
    it('Single K', () => {
        expect(calcPoints(["S6", "S7", "S8", "SK"], "E")).to.equal(4)
    })
    it('Single A', () => {
        expect(calcPoints(["S6", "S7", "S8", "SA"], "E")).to.equal(11)
    })
})


// TODO: cardAllowed: untertrumpfen
describe('CardAllowed', () => {
    it('Test 1', () => {
        expect(cardAllowed(["EU", "EA"], "E9", ["E9", "H6", "H9", "HO", "HA", "L6", "L7"], "H")).to.equal(true)
    })
    it('No Bauer angabe if only trumpf in trumpf opening', () => {
        expect(cardAllowed(["EO", "EA"], "S6", ["S6", "EU"], "E")).to.equal(true)
    })
    it('No Bauer angabe if only trumpf in trumpf opening but wrong color', () => {
        expect(cardAllowed(["LO", "LA"], "S6", ["S6", "EU", "L6"], "E")).to.equal(false)
    })
    it('Domis case', () => {
        expect(cardAllowed(["SA", "HK"], "HU", ["EA", "HU", "L6", "LK", "LA", "S8", "SU"], "H")).to.equal(true)
    })
})

// TODO: compareWeis: Q and 5
describe('CompareWeis', () => {
    it('4 Straight vs 3 Straight', () => {
        expect(compareWeis(["4SK"], ["3S9"], "E")).to.equal(1)
    })
    it('3 Straight vs 4 Straight', () => {
        expect(compareWeis(["3SK"], ["4S9"], "E")).to.equal(-1)
    })
    it('3 Straight vs 3 Straight same value', () => {
        expect(compareWeis(["3SK"], ["3LK"], "E")).to.equal(0)
    })
    it('Bennis Case', () => {
        expect(compareWeis(["3SK"], ["3S9"], "S")).to.greaterThan(0)
    })
    it('Bennis Case without trumpf', () => {
        expect(compareWeis(["3SK"], ["3S9"], "E")).to.greaterThan(0)
    })
    it('Adis Case', () => {
        expect(compareWeis(["3LO"], ["3E9", "3SX"], "KO")).to.greaterThan(0)
    })
})