import { expect, use, should } from 'chai'
import ChaiArrays = require('chai-arrays');
import 'mocha';
import {sort, cardsToWeis, toWeis} from '../jasslogic'



function newHandCards(): Array<Array<string>> {
    const cards = [
        "S6", "S7", "S8", "S9", "SX", "SU", "SO", "SK", "SA",
        "H6", "H7", "H8", "H9", "HX", "HU", "HO", "HK", "HA",
        "L6", "L7", "L8", "L9", "LX", "LU", "LO", "LK", "LA",
        "E6", "E7", "E8", "E9", "EX", "EU", "EO", "EK", "EA"
    ]
    const hands = []
    for (let i = 0; i < 4; i++) {
        hands[i] = []
        for (let j = 0; j < 9; j++) {
            hands[i].push(cards.splice(Math.floor(Math.random()*(cards.length)), 1)[0])
        }
        sort(hands[i])
    }
    return hands
}

describe('Amount of weises', () => {
    it('Quartett', () => {
        let amountQ = 0
        let amountS = 0
        let amount3 = 0
        let amount4 = 0
        let amount5 = 0
        let amount6 = 0
        let amount7 = 0
        let amount8 = 0
        let amount9 = 0
        const n = 10000
        for (let i = 0; i < n; i++) {
            const hands = newHandCards()
            for (let j = 0; j < 4; j++) {
                const weis = cardsToWeis(hands[j], "S")
                amountQ += weis.some(w => toWeis(w).rank === 'Q') ? 1 : 0
                amountS += weis.some(w => toWeis(w).rank === 'S') ? 1 : 0
                amount3 += weis.some(w => toWeis(w).rank === '3') ? 1 : 0
                amount4 += weis.some(w => toWeis(w).rank === '4') ? 1 : 0
                amount5 += weis.some(w => toWeis(w).rank === '5') ? 1 : 0
                amount6 += weis.some(w => toWeis(w).rank === '6') ? 1 : 0
                amount7 += weis.some(w => toWeis(w).rank === '7') ? 1 : 0
                amount8 += weis.some(w => toWeis(w).rank === '8') ? 1 : 0
                amount9 += weis.some(w => toWeis(w).rank === '9') ? 1 : 0
            }
        }

        console.info(`Amount Q: ${amountQ}`)
        console.info(`Amount S: ${amountS}`)
        console.info(`Amount 3: ${amount3}`)
        console.info(`Amount 4: ${amount4}`)
        console.info(`Amount 5: ${amount5}`)
        console.info(`Amount 6: ${amount6}`)
        console.info(`Amount 7: ${amount7}`)
        console.info(`Amount 8: ${amount8}`)
        console.info(`Amount 9: ${amount9}`)
    })
})