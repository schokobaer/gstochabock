# GstochaBock

#### Principles
  - The platform should be kept as universally as possible
  - The players have to communicate additionaly on a different platform
  - It is not considered to make it work on an iPhone or on any mobile platforms

#### Definitions
  - COLOR = {E, L, H, S}
  - VALUE = {6, 7, 8, 9, X, U, O, K, A}
  - WEISRANK = {S (=Stoecke), Q (=Quartett), 3, 4, 5, 6, 7, 8, 9}
  - TRUMPF = {E, L, H, S, G, B, KU, KO}

#### Cards representation:
2 characters in upper case in the form [COLOR,VALUE]

### Weis representation:
  - Stoecke has the single character S
  - Quartett has 2 characters in the form ["Q", VALUE]
  - Straight has 3 characters in the form [WEISRANK,CARD], where WEISRANK defines the length of the row and CARD defines the highest card in the straight

Examples:
  - S: Stoecke
  - QU: a quartett of Bauer
  - QK: a quartett of Kings
  - 3SO: a straight of three in Schell {SO, SU, SX}
  - 4EA: a straight of four in Eichel {EA, EK, EO, EU}
  - 5HX: a straight of five in Herz {HX, H9, H8, H7, H6}

## Scenario
  - On opening the page the player name has to be prompted. After a valid name was entered, the FCM messaging token is requested and a 16 byte playerid encoded in base64 is generated. Everything is saved in the localStorage and the fmc is sent to /table/subscribe to subscribe to open table updates on the table view.
  - First a table is created. If the request body contains a password, the password is set and always has to be fullfilled in each POST request. Also the player which creates the table gets joined automatically on position 0
  - To start a game, a table has to contain exactly 4 players. A player can join a table with a join request. The request needs to contain the position, so the server knows where to put the player. After the 4th player joined the table, the cards get shuffled and released to the players.
  - To start a round one player of the table has to define a trumpf with the trumpf request. This has to be done in the beginning of each new game. The player which makes the trumpf request also starts the first round.
  - After the trumpf request and before the first play request of a game each player can execute a weis request. In the weis request the cards of the weis have to be defined. A second weis request would override the first one. The weis request is only accepted, if the player has 9 cards on hand
  - When a player plays a card, it is checked if the player is allowed to play that card. If it was the last player of the current round, the points of the round get calculated and the player which won the round.
  - Before the next round can start, either a clean request has to be done of one of the players on the table or a timeout invokes the clean. If it was the last round of the game, a new game is initialized.
  - To inform the frontend about changes, FCM topic messages are used to notify them. When the frontend receives a FCM message (either 'TABLE' or 'TABLE.${ID}) an extra GET request has to be done, to get the payload, becaue each player should receive different data (not every player should know all the hand cards e.g.)

## CMDs
#### Start functions localy
```
cd ./functions
npm run build
firebase serve --only functions
```
#### Start frontend
```
cd ./webapp
npm start
```
#### Deploy on firebase
```
cd ./webapp
npm run build                 # Build the webapp
cp -r ./public/* ../public    # Copy the compiled webapp to the firebase hosting folder
cd ..
firebase deploy               # Deploy firebase project
```



## API:
  - GET /table
  - GET /table/{id}?playerid=id
  - POST /table/subscribe
  - POST /table/create {playerid: id, name: string, password?: string, fcm: string}
  - POST /table/{id}/join {playerid: id, name: string, position: int, password?: string, fcm: string}
  - POST /table/{id}/trumpf {playerid: id, password?: string, trumpf: string}
  - POST /table/{id}/weis {playerid: id, password?: string, cards: Array<string>}
  - POST /table/{id}/play {playerid: id, password?: string, card: string}
  - POST /table/{id}/clean {playerid: id, password?: string}
  - POST /table/{id}/undo {playerid: id, password?: string}


## Table data structure
```
{
    password: null,
    points: [0, 0],
    currentMove: 0,
    trumpf: "S",
    round: ["s6", "su", "l4"],
    matschable: true,
    history: null,
    players: [
        {
            playerid: "alskjflsdafkj",
            name: "Hans",
            position: 0,
            weis: ["3SU"],
            cards: ["sx", "so", "sa", "l7", lx", "la", "hu", "ho"]
        },
        {
            playerid: "kjdleijkfldliejvkl",
            name: "Otto",
            position: 1,
            weis: [],
            cards: ["sx", "so", "sa", "l7", lx", "la", "hu", "ho"]
        },
        {
            playerid: "eijviejivnei",
            name: "Joko",
            position: 2,
            weis: [],
            cards: ["sx", "so", "sa", "l7", lx", "la", "hu", "ho"]
        },
        {
            playerid: "eivlaivlaaaaaa",
            name: "Fritz",
            position: 3,
            weis: [],
            cards: ["sx", "so", "sa", "l7", lx", "la", "hu", "ho"]
        }
    ]
}
```