import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express'
import { cardAllowed, calcPoints, roundWinner, sort, cardsToWeis, trumpfOrder, compareWeis, calcWeisPoints } from 'gstochabock-core'
import {Player, Table} from './entities'
import {PlayerDto, TableDto, GameDto, CreateRequestBody, CreateResponseBody, JoinRequestBody, TrumpfRequestBody, WeisRequestBody, StoeckeRequestBody, PlayRequestBody, UndoRequestBody, NewRequestBody} from 'gstochabock-core'
const serviceAccount = require("../serviceAccountKey.json");

const TABLE_COLLECTION = 'tables';

function nextGame(table: Table): Table {
    const cards = [
        "S6", "S7", "S8", "S9", "SX", "SU", "SO", "SK", "SA",
        "H6", "H7", "H8", "H9", "HX", "HU", "HO", "HK", "HA",
        "L6", "L7", "L8", "L9", "LX", "LU", "LO", "LK", "LA",
        "E6", "E7", "E8", "E9", "EX", "EU", "EO", "EK", "EA"
    ]
    table.currentMove = null
    table.points[0] = table.points[1] = 0
    table.weisPoints[0] = table.weisPoints[1] = 0
    table.round = []
    table.lastRound = null
    table.matschable = null
    table.history = null
    table.trumpf = null
    for (let i = 0; i < 4; i++) {
        table.players[i].cards = [] // trivial, becaue the hand should be empty
        for (let j = 0; j < 9; j++) {
            table.players[i].cards.push(cards.splice(Math.floor(Math.random()*(cards.length)), 1)[0])
        }
        table.players[i].cards = sort(table.players[i].cards)
        table.players[i].stoeckeable = null
    }
    return table
}

/**
 * Adapts the data from the firestore to fit the current structure.
 * The function adds unset attributes, or changes values
 * @param data the data of the documentstore
 */
function tableMigration(data: any): Table {
    interface Matschable {
        matschable?: Boolean | null
    }
    const dataMatsch = data as Matschable
    if (dataMatsch.matschable === undefined) {
        dataMatsch.matschable = null
    }
    interface Historyable {
        history?: string | null
    }
    const dataHistory = dataMatsch as Historyable
    if (dataHistory.history === undefined) {
        dataHistory.history = null
    }
    
    return dataHistory as Table
}

function startTableTransaction(docId: string, res: functions.Response<any>): Promise<[FirebaseFirestore.Transaction, Table]> {
    return new Promise((resolve, reject) => {
        const docRef = admin.firestore().collection(TABLE_COLLECTION).doc(docId)
        admin.firestore().runTransaction(tx => {
            return tx.get(docRef).then(doc => {
                if (!doc.exists) {
                    res.status(404).send()
                    console.warn('Table not found ' + docId)
                    reject('Table not found')
                }

                const table = doc.data() as Table
                resolve([tx, table])
            })
            .catch(err => {
                console.error('Could not read table', err)
                res.status(500).send(err)
                reject(err)
            })
        })
    })
}

function writeTableTransaction(tx: FirebaseFirestore.Transaction, docId: string, table: Table) {
    const docRef = admin.firestore().collection(TABLE_COLLECTION).doc(docId)
    tx.set(docRef, table)
}

function loadTable(docId: string, res: functions.Response<any>): Promise<Table> {
    return new Promise((resolve, reject) => {
        
        admin.firestore().collection(TABLE_COLLECTION).doc(docId).get()
        .then(doc => {
            if (!doc.exists) {
                res.status(404).send()
                console.warn('Table not found ' + docId)
                reject('Table not found')
            }
            const table = tableMigration(doc.data())
            resolve(table)
        })
        .catch(err => {
            console.error('Could not read table', err)
            res.status(500).send(err)
            reject(err)
        })
    })
}

function writeTable(docId: string, table: Table, res: functions.Response<any>): Promise<void> {
    //TODO?: Attach lastModified attribute to table
    return new Promise((resolve, reject) => {
        admin.firestore().collection(TABLE_COLLECTION).doc(docId).set(table, {})
        .then(k => {
            resolve()
        }).catch(err => {
            console.error('Could not write table', err)
            res.status(500).send(err)
            reject(err)
        })
    })
}


function getPlayer(table: Table, playerid: string, res: functions.Response<any>): Player | null {
    const player = table.players.find(p => p.playerid === playerid)
    if (!player) {
        console.warn('Invalid player ' + playerid)
        res.status(403).send()
        return null
    }
    return player
}

function notifyTable(docId?: string) {
    return admin.database().ref(docId ? `games/${docId}` : 'table').set(new Date().getTime())
}


/**
 * Creates a deep copy of the table without the history and than saves that deepcopy stringified in the history field.
 * @param table the table to save the current state in its history
 */
function addHistory(table: Table): Table {
    table.history = null // To achieve that only one single history is available
    const copy = <Table>JSON.parse(JSON.stringify(table))
    table.history = JSON.stringify(copy)
    return table
}

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://gstochabock.firebaseio.com"
})

// Init express
const app = express()
const router = express.Router()



/**
 * Delivers all open.
 */
router.get('/', (req, res) => {
    admin.firestore().collection(TABLE_COLLECTION).get()
    .then(snapshot => {
        const tables: Array<TableDto> = snapshot.docs.filter(doc => doc.exists && (doc.data() as Table).players.length < 4).map(doc => {
            const table = doc.data() as Table
            const dto: TableDto = {
                id: doc.id,
                protected: table.password !== null,
                players: table.players.map(p => {return {name: p.name, position: p.position}})
            }
            return dto
        })
        res.send(tables)
    }).catch(err => {
        console.error('Could not list table docments: ', err)
        res.status(500).send(err)
    })
})

/**
 * Delivers all data in the view of the player. If player has no access rightsr, a simple TableDto is returned.
 * So the player won't see other players cards, nor the points if the game is not over.
 */
router.get('/:id', (req, res) => {
    const docId = req.params.id
    const playerid = req.query.playerid

    loadTable(docId, res)
    .then(table => {
        const player = table.players.find(p => p.playerid === playerid)
        if (!player) {
            const result: TableDto = {
                id: docId,
                players: table.players.map(p => {return {name: p.name, position: p.position}}),
                protected: table.password !== null
            }
            res.send(result)
            return
        }
        const sendWeis = table.players.filter(p => p.cards.length === 8).length === 4
        const result: GameDto = {
            players: [],
            currentMove: table.currentMove !== null ? table.currentMove : undefined,
            round: table.round,
            lastRound: table.lastRound || undefined,
            trumpf: table.trumpf || undefined,
            points: table.players.filter(p => p.cards.length === 0).length === 4 && 
                    table.round.length === 0 && 
                    table.trumpf !== null ? table.points : undefined,
            weisPoints: table.players.filter(p => p.cards.length === 0).length === 4 && 
                        table.round.length === 0 && 
                        table.trumpf !== null ? [
                            {
                                points: table.weisPoints[0],
                                stoecke: table.players[0].stoeckeable === true || table.players[2].stoeckeable === true
                            },
                            {
                                points: table.weisPoints[1],
                                stoecke: table.players[1].stoeckeable === true || table.players[3].stoeckeable === true
                            }
                        ] : undefined,
            undoable: table.history !== null
        }
        result.players.push({
            name: player.name,
            position: player.position,
            cards: player.cards || undefined,
            weis: sendWeis && player.weis !== null ? player.weis : undefined
        })
        for (let i = 1; i <= 3; i++) {
            const p = table.players.find(p => p.position === (player.position + i) % 4)
            let pDto: PlayerDto | null = null
            if (p) {
                pDto = {
                    name: p.name,
                    position: p.position
                }
                if (sendWeis && p.weis.length > 0) {
                    pDto.weis = p.weis
                }
            }
            result.players.push(pDto)
        }
        res.send(result)
    }).catch()
})


/**
 * Creates a new table and sets the creating player on position 0
 */
router.post('/create', (req, res) => {
    const data = req.body as CreateRequestBody
    const doc = admin.firestore().collection(TABLE_COLLECTION).doc()
    const docId = doc.id;
    const table: Table = {
        password: data.password || null,
        points: [0, 0],
        weisPoints: [0, 0],
        currentMove: null,
        trumpf: null,
        round: [],
        lastRound: null,
        matschable: null,
        history: null,
        players: [
            {
                playerid: data.playerid,
                name: data.name,
                position: 0,
                weis: [],
                cards: [],
                stoeckeable: null
            }
        ]
    }
    writeTable(docId, table, res).then(() => {
        console.log('Table created with id ' + docId)
        notifyTable()
        .catch(err => console.error(`Could not notify table: ${err}`))
        .finally(() => {
            const dto: CreateResponseBody = {
                id: docId
            }
            res.send(dto)
        })
    }).catch()
})

/**
 * Adds the player to the table. If it is the 4th player, nextGame is called.
 */
router.post('/:id/join', (req, res) => {
    const docId = req.params.id
    const data = req.body as JoinRequestBody
    startTableTransaction(docId, res)
    .then(p => {
        const tx = p[0]
        const table = p[1]
        if (table.password !== (data.password || null)) {
            res.status(403).send()
            console.warn(`Wrong password in table ${docId}`)
            return
        }
        if (table.players.length > 4) {
            res.status(407).send()
            console.warn('Could not join table because it is full ' + docId)
            return
        }
        if (table.players.some(p => p.position === data.position)) {
            res.status(407).send()
            console.warn('Could not join table because position is already set ' + docId)
            return
        }
        if (table.players.some(p => p.name === data.name)) {
            // TODO?: Should it be possible to have two players with the same name on one table?
            res.status(407).send()
            console.warn('Could not join table because name is already set ' + docId)
            return
        }
        table.players.push({
            playerid: data.playerid,
            name: data.name,
            position: data.position,
            weis: [],
            cards: [],
            stoeckeable: null
        })
        table.players.sort((x,y) => x.position - y.position)
        if (table.players.length === 4) {
            nextGame(table)
            console.info('Table is full ' + docId)
        }
        writeTableTransaction(tx, docId, table)
        console.info(`${data.playerid} joined table ${docId}`)
        Promise.all([
            notifyTable().catch(err => console.error(`Could not notify table: ${err}`)),
            notifyTable(docId).catch(err => console.error(`Could not notify game: ${err}`))
        ]).finally(() => res.send())
    }).catch()
})

/**
 * Sets the trumpf for the game. Only possible in the beginning
 */
router.post('/:id/trumpf', (req, res) => {
    console.info('Starting trumpf')
    const docId = req.params.id
    const data = req.body as TrumpfRequestBody
    loadTable(docId, res)
    .then(table => {
        if (!getPlayer(table, data.playerid, res)) {
            return
        }
        if (table.players.length !== 4 || table.trumpf !== null) {
            console.warn('Could set trumpf ' + docId)
            res.status(407).send()
            return
        }
        if (trumpfOrder.indexOf(data.trumpf) < 0) {
            console.warn('Invalid trumpf ' + data.trumpf)
            res.status(407).send()
            return
        }

        addHistory(table)
        table.trumpf = data.trumpf
        table.currentMove = table.players.find(p => p.playerid === data.playerid)!.position

        //Stoeckable
        for (let i = 0; i < 4; i++) {
            if (table.players[i].cards.indexOf(data.trumpf + "O") >= 0 && table.players[i].cards.indexOf(data.trumpf + "K") >= 0) {
                table.players[i].stoeckeable = false
                console.info(`${table.players[i].name} is stoeckable`)
            }
        }

        writeTable(docId, table, res).then(() => {
            console.info(`${data.playerid} set trumpf to ${data.trumpf} on table ${docId}`)
            notifyTable(docId).finally(() => res.send())
        }).catch()
    }).catch()
})

/**
 * Sets the weis for a player.
 */
router.post('/:id/weis', (req, res) => {
    console.info('Starting weis')
    const docId = req.params.id
    const data = req.body as WeisRequestBody
    startTableTransaction(docId, res)
    .then(p => {
        const tx = p[0]
        const table = p[1]
        const player = getPlayer(table, data.playerid, res)
        if (!player) return
        
        const containsCards = data.cards.reduce((valid, card) => valid && player.cards.indexOf(card) >= 0, true)
        if (!containsCards) {
            console.warn('Weis cards are not on hand from ' + data.playerid)
            res.status(407).send()
            return
        }

        if (player.cards.length < 9) {
            console.warn('Can not weis because its not the first round of the game ' + data.playerid)
            res.status(407).send()
            return
        }
        
        player.weis = cardsToWeis(data.cards, table.trumpf)
        writeTableTransaction(tx, docId, table)
        console.info(`${data.playerid} made a weis on table ${docId}`)
        res.send()
    }).catch()
})

router.post('/:id/stoecke', (req, res) => {
    const docId = req.params.id
    const data = req.body as StoeckeRequestBody
    startTableTransaction(docId, res)
    .then(p => {
        const table = p[1]
        const player = getPlayer(table, data.playerid, res)
        if (!player) return
        
        if (player.stoeckeable === false) {
            player.stoeckeable = true
        }

        writeTableTransaction(p[0], docId, table)
        console.info(`${player.name} indicated stoecke on table ${docId}`)
        res.send()
    }).catch()
})

/**
 * Plays a card from a player on the table.
 * If it was the 4th card, the poins get calculated and the opening
 * player for the next round is determined.
 */
router.post('/:id/play', (req, res) => {
    console.info('Starting play')
    const docId = req.params.id
    const data = req.body as PlayRequestBody
    startTableTransaction(docId, res)
    .then(p => {
        const tx = p[0]
        const table = p[1]
        const player = getPlayer(table, data.playerid, res)
        if (!player) return

        if (player.position !== table.currentMove) {
            console.warn('Not the current move for ' + data.playerid)
            res.status(407).send()
            return
        }

        // Reset weis, if it is the first card of second round
        if (table.players.filter(p => p.cards.length === 8).length === 4) {
            console.info('Clearing all weises')
            for (let i = 0; i < 4; i++) {
                table.players[i].weis = []
            }
        }

        if (!cardAllowed(table.round, data.card, player.cards, table.trumpf!)) {
            console.warn('Card not allowed at this point of the round')
            res.status(407).send()
            return
        }

        table.history = null
        table.round.push(data.card);
        player.cards.splice(player.cards.indexOf(data.card), 1)
        table.currentMove = (table.currentMove + 1) % 4

        // last round -> calc points and nextMove
        if (table.round.length === 4) {
            // if it was the last card of the first round, find the best weis, remove the others, calc weis points and remove a possible Stoecke
            let bestWeisIdx = undefined
            for (let i = 0; i < 4; i++) {
                const realIdx = (i + table.currentMove + 4) % 4
                if (table.players[realIdx].weis.length > 0) {
                    if (bestWeisIdx === undefined) {
                        bestWeisIdx = realIdx
                        console.info(`The first weis has position ${realIdx}`)
                    } else {
                        if (compareWeis(table.players[realIdx].weis, table.players[bestWeisIdx].weis, table.trumpf!) > 0) {
                            bestWeisIdx = realIdx
                            console.info(`Position ${realIdx} had a better weis`)
                        }
                    }
                }
            }
            if (bestWeisIdx !== undefined) {
                console.info(`Best weis was position ${bestWeisIdx}`)
                if (bestWeisIdx % 2 === 0) {
                    table.players[1].weis = []
                    table.players[3].weis = []
                } else {
                    table.players[0].weis = []
                    table.players[2].weis = []
                }
            }
            if (table.weisPoints[0] + table.weisPoints[1] === 0) {
                for (let i = 0; i < 4; i++) {
                    table.weisPoints[i % 2] += calcWeisPoints(table.players[i].weis)
                }
            }

            // store last round
            table.lastRound = {
                startPosition: table.currentMove,
                cards: table.round as [string, string, string, string]
            }

            // make round calculations
            // finished round
            const nextMove = roundWinner(table.round, table.trumpf!)
            let points = calcPoints(table.round, table.trumpf!)

            // Check matschable
            if (table.matschable === null) {
                table.matschable = true
            } else if (table.matschable === true && (nextMove % 2) === 1) {
                table.matschable = false
            }

            if (player.cards.length === 0) {
                // last stich
                points += 5
                if (table.matschable === true) {
                    points += 100
                    console.info(`Matsch on table ${docId}`)
                }
            }
            
            table.points[(table.currentMove! + nextMove) % 2] += points
            table.currentMove = (table.currentMove! + nextMove) % 4
            console.info(`${table.players[nextMove].playerid} made ${points} points on table ${docId}`)

            table.round = []

            // Flip Kulmi
            table.trumpf = table.trumpf === "KU" ? "KO" :
                       table.trumpf === "KO" ? "KU" : table.trumpf
        }

        writeTableTransaction(tx, docId, table)
        console.info(`${data.playerid} played ${data.card} on table ${docId}`)
        notifyTable(docId).finally(() => res.send())
    }).catch()
})

/**
 * Reverts the last change if an old state is stored in the history.
 */
router.post('/:id/undo', (req, res) => {
    const docId = req.params.id
    const data = req.body as UndoRequestBody
    loadTable(docId, res)
    .then(table => {        
        const player = getPlayer(table, data.playerid, res)
        if (!player) return

        if (table.history === null) {
            console.warn('No history available ' + docId)
            res.status(407).send()
            return
        }

        // reset last change
        const historyTable = JSON.parse(table.history) as Table

        writeTable(docId, historyTable, res).then(() => {
            console.info(`${data.playerid} has executed an undo in table ${docId}`)
            notifyTable(docId).finally(() => res.send())
        }).catch()
    }).catch()
})

/**
 * Initiates a new game, if all hand cards are empty. State is stored in history.
 */
router.post('/:id/new', (req, res) => {
    const docId = req.params.id
    const data = req.body as NewRequestBody
    loadTable(docId, res)
    .then(table => {        
        const player = getPlayer(table, data.playerid, res)
        if (!player) return

        if (table.players.filter(p => p.cards.length === 0).length !== 4 || table.round.length !== 0) {
            console.warn('New game can not be initialized at this point at table ' + docId)
            res.status(407).send()
            return
        }
        
        addHistory(table)
        const history = table.history
        nextGame(table)
        table.history = history

        writeTable(docId, table, res).then(() => {
            console.info(`${data.playerid} has started a new game on table ${docId}`)
            notifyTable(docId).finally(() => res.send())
        }).catch()
    }).catch()
})

// Assign micro funtion to express app
app.use('/table', router)

export const api = functions.https.onRequest(app)