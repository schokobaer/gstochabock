import {Position, Trumpf} from 'gstochabock-core'

export interface Player {
    playerid: string
    name: string
    position: Position
    weis: Array<string>
    cards: Array<string>
    stoeckeable: boolean | null
}

export interface  Table {
    password: string | null
    points: [number, number]
    weisPoints: [number, number]
    currentMove: number | null
    trumpf: Trumpf | null
    round: Array<string>
    lastRound: {
        startPosition: number
        cards: [string, string, string, string]
    } | null
    matschable: boolean | null
    players: Array<Player>
    history: string | null
}
