import React, { Fragment } from 'react';
import './App.css';
import TablePage from './TablePage';
import GamePage from './GamePage';
import * as firebase from 'firebase'
import { uuid } from 'uuidv4'

class App extends React.Component<Props, State> {

  private dbRef?: firebase.database.Database

  state: State = {
    tableId: undefined
  }

  initFirebase() {
    const firebaseConfig = {
      apiKey: "AIzaSyD6LlrgEIEjf0tM4GL8u_IHvWbQd0KZZ5E",
      authDomain: "gstochabock.firebaseapp.com",
      databaseURL: "https://gstochabock.firebaseio.com",
      projectId: "gstochabock",
      storageBucket: "gstochabock.appspot.com",
      messagingSenderId: "22390576193",
      appId: "1:22390576193:web:c3be3bb14f2aacc56ee2c0"
    }
    /* const firebaseConfig = {
      messagingSenderId: "22390576193"
    } */
    firebase.initializeApp(firebaseConfig)
    this.dbRef = firebase.database()

    /* firebase.database().ref('games/abcd').on('value', snapshot => {
      console.info('abcd game has changed... lets fetch the new data')
    }) */
  }

  getTableId = () => document.location.hash.length < 2 ? undefined : document.location.hash.substring(1)
  
  componentWillMount() {

    this.setState({tableId: this.getTableId()})
    window.onhashchange = () => {
      this.setState({tableId: this.getTableId()})
    }
    if (window.localStorage.name) {
      this.initFirebase()
    }
  }

  render () {
    while (window.localStorage.getItem("name") === null) {
      let name = prompt("Name eingeben");
      if (name !== null && name.length > 0 ){
        window.localStorage.name = name
        window.localStorage.playerid = uuid()
        this.initFirebase()
      }
    }

    if (!this.dbRef) {
      return <div>Fetching data ...</div>
    }

    const howto = <div className="app-logout">
      <a href="howto.html" className="jass-btn">HowTo</a>
    </div>

    if (this.state.tableId) {
      return <div>
        {howto}
        <GamePage tableId={this.state.tableId} dbRef={this.dbRef} />
      </div>
    }

    return <Fragment>
        {howto}
        <TablePage dbRef={this.dbRef} />    
      </Fragment>
  }
}

interface Props {}
interface State {
  tableId?: string
}

export default App;
